# DeepCreamPy
*Decensoring Hentai with Deep Neural Networks.*

A deep learning-based tool to automatically replace censored artwork in hentai with plausible reconstructions.

Before DeepCreamPy can be used, the user must color censored regions in their hentai green with an image editing program (e.g. GIMP, Photoshop). DeepCreamPy takes the green colored images as input, and a neural network automatically fills in the censored regions.

You can download the latest release for Windows 64-bit [here](https://gitlab.com/Zneden/dcp/-/releases).

For users interested in compiling DeepCreamPy themselves, DeepCreamPy can run on Windows, Mac, and Linux.


## Features
- Decensoring images of any size
- Decensoring of ANY shaped censor (e.g. black lines, pink hearts, etc.)
- Decensoring of mosaic decensors
- Limited support for decensoring black and white/monochrome images
- Generate multiple variations of decensors from the same image

## Limitations
The decensorship is for color hentai images that have minor to moderate censorship of the human reproductive organs. If an organ is completely censored out, decensoring will be ineffective.

It does NOT work with:
- Hentai with screentones (e.g. printed hentai)
- Real life pornographic material
- Censorship of nipples
- Censorship of lower orifice of the alimentary canal
- Animated gifs/videos

## Install & How to use
Note: I killed the GUI to allow usage on bash-only OS/VMs.

Short version:
1. Install conda with python 3.6.7
2. Install requirements using pip
3. run decensor.py with python

Long version, for beginners (full, lazy copy-paste style commands for u):
- sudo apt update
- sudo apt install -y libsm6 libxext6
- sudo apt-get install -y libxrender-dev
- wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
- chmod +x Anaconda3-2021.05-Linux-x86_64.sh
- ./Anaconda3-2021.05-Linux-x86_64.sh

(accept licence, leave everything on default)

- RESTART YOUR BASH (if on ssh, just logout and login again)
- conda config --add channels conda-forge
- conda create --name DCP python=3.6.7

(confirm 'yes' when asked if you want to proceed)

- conda activate DCP
- pip install -r requirements-cpu.txt

To run:

python decensor.py (bar censor)

python decensor.py --is_mosaic true (mosaic censor)

python decensor.py --variations 1,2,4 (number of results. The AI will decensor it different ways so you can pick the best result. Only input 1, 2 or 4)


Of course you can combine variations & mosaic.


For script & automation purposes:

See bar.sh and mosaic.sh. These bash scripts contains code to activate your conda environment and run the decensor. Just change the path to match ur setup.


## License
Source code and official releases/binaries are distributed under the [GNU Affero General Public License v3.0](LICENSE.md).

## Acknowledgements
Neural network code is modified from Forty-lock's project [PEPSI](https://github.com/Forty-lock/PEPSI), which is the official implementation of the paper [PEPSI : Fast Image Inpainting With Parallel Decoding Network](http://openaccess.thecvf.com/content_CVPR_2019/html/Sagong_PEPSI__Fast_Image_Inpainting_With_Parallel_Decoding_Network_CVPR_2019_paper.html). [PEPSI](https://github.com/Forty-lock/PEPSI) is licensed under the MIT license.

Training data is modified from gwern's project [Danbooru2017: A Large-Scale Crowdsourced and Tagged Anime Illustration Dataset](https://www.gwern.net/Danbooru2017) and other sources.

This repo is a clone of gguilt's DeepCreamPy repo. I just modified the decensor.py so it can be run from script on a non-graphic OS (this probably broke the main.py, and definitely killed the GUI version).
